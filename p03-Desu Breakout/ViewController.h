//
//  ViewController.h
//  breakout
//
//  Created by Anuroop on 2/11/16.
//
#import <UIKit/UIKit.h>
#import "GameScreen.h"

@interface ViewController : UIViewController
@property (nonatomic, strong) IBOutlet GameScreen *gameScreen;
@property (nonatomic, strong) IBOutlet UIButton *res;
- (IBAction)resets;
@end

