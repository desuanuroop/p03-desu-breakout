//
//  GameScreen.h
//  breakout
//
//  Created by Anuroop on 2/11/16.
//

#import <UIKit/UIKit.h>

@interface GameScreen : UIView
{
	float dx, dy;  // Ball motion
    int tscore;
}
@property (nonatomic, strong) UIView *paddle;
@property (nonatomic, strong) UIView *ball;
@property (nonatomic, strong) NSTimer *timer;
@property (nonatomic, strong) NSMutableArray *rectangles;
@property (nonatomic, strong) NSMutableArray *lives;
@property (nonatomic, strong) UILabel *score;

-(void)createPlayField;
-(void)checkBlocks;
@end
