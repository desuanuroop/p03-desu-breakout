//
//  main.m
//  p03-Desu Breakout
//
//  Created by Anuroop Desu on 2/11/16.
//  Copyright © 2016 Anuroop Desu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
