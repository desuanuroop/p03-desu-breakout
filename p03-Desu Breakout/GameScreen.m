//
//  GameScreen.m
//  breakout
//
//  Created by Anuroop on 2/11/16.
// 
//

#import "GameScreen.h"

@implementation GameScreen
@synthesize paddle, ball;
@synthesize timer;
@synthesize lives;
@synthesize score;
-(void)createPlayField
{
    
    self.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"breakout_bg.png"]];
	
    paddle = [[UIView alloc] initWithFrame:CGRectMake(135.5, 340, 80, 5)];
    paddle.layer.cornerRadius = 4.0;
    paddle.layer.masksToBounds = YES;
    paddle.backgroundColor = [UIColor whiteColor];
	[self addSubview:paddle];
	
	ball = [[UIView alloc] initWithFrame:CGRectMake(165, 326, 10, 10)];
    ball.layer.cornerRadius = 5.0;
    ball.layer.masksToBounds = YES;
	[self addSubview:ball];
	[ball setBackgroundColor:[UIColor redColor]];
    
    self.rectangles = [[NSMutableArray alloc] init];
    for(int row = 0; row<7 ;row++)
    for(int col=0; col<8; col++){
        UIView *v = [[UIView alloc] initWithFrame:CGRectMake( 20+(40 * col), 40 + (20 * row), 30, 10)];
        if(row % 2 ==0)
            v.backgroundColor = [UIColor blueColor];
        else
            v.backgroundColor = [UIColor yellowColor];
        [self addSubview:v];
        [self.rectangles addObject:v];
    }
    
    self.lives = [[NSMutableArray alloc] init];
    for (int i=0; i<3;i++) {
        
        UIView *life = [[UIView alloc] initWithFrame:CGRectMake(5 + (12*i), 15, 10, 10)];
        life.layer.cornerRadius = 5.0;
        [life setBackgroundColor:[UIColor redColor]];
        [self addSubview:life];
        [self.lives addObject:life];
    }
    
    score = [[UILabel alloc]initWithFrame:CGRectMake(250, 3, 100, 30)];
    score.textColor = [UIColor whiteColor];
    [score setText:[NSString stringWithFormat:(@"score: 0")]];
    [self addSubview:score];
    
    tscore = 0;
	dx = 10;
	dy = 10;
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
	for (UITouch *t in touches)
	{
		CGPoint p = [t locationInView:self];
        CGPoint new;
        new.x = p.x;
        new.y = 340;
		[paddle setCenter:new];
	}
}

-(void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
	[self touchesBegan:touches withEvent:event];
}

-(IBAction)startAnimation:(UIButton *)sender
{
    timer = [NSTimer scheduledTimerWithTimeInterval:0.1	target:self selector:@selector(timerEvent:) userInfo:nil repeats:YES];
}

-(IBAction)stopAnimation:(id)sender
{
    
	[timer invalidate];
}

-(void)timerEvent:(id)sender
{
	CGRect bounds = [self bounds];
	
	// NSLog(@"Timer event.");
	CGPoint ballc = [ball center];
    CGPoint paddlec = [paddle center];
    
    if(ballc.y > 341) {
        
        int i = (int)self.lives.count;
        if(i != 0) {
            UIView *life = self.lives[i-1];
            [life removeFromSuperview];
            [self.lives removeObject:life];
        }
        else{
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"OOPS ! No More Lives"
                                                           message: @"End of Play"
                                                          delegate: self
                                                 cancelButtonTitle:@ "Reset"
                                                 otherButtonTitles:@"Exit",nil];
            [alert show];
        }
        ballc.x = 169;
        ballc.y = 337;
        [ball setCenter:ballc];
        paddlec.x = 165;
        paddlec.y = 340;
        
        [paddle setCenter:paddlec];
        
        [self stopAnimation:self];
    }
    if ((ballc.x + dx) < 0)
		dx = -dx;
	
	if ((ballc.y + dy) < 0)
		dy = -dy;
	
	if ((ballc.x + dx) > bounds.size.width || (ballc.x + dx) <= 0)
		dx = -dx;
	
	if ((ballc.y + dy) > bounds.size.height || (ballc.y + dy) <= 0)
		dy = -dy;
	
	ballc.x += dx;
	ballc.y += dy;
	[ball setCenter:ballc];
	
	// Now check to see if we intersect with paddle.  If the movement
	// has placed the ball inside the paddle, we reverse that motion
	// in the Y direction.
	if (CGRectIntersectsRect([ball frame], [paddle frame]))
	{
		dy = -dy;
        ballc.y += 2*dy;
		[ball setCenter:ballc];
        
	}
    
    [self checkBlocks];
}
- (void)checkBlocks {
    
    CGPoint p = [ball center];
    for(int i=(int)self.rectangles.count-1; i >=0; i--){
        
        UIView *block = self.rectangles[i];
        
        if (CGRectIntersectsRect([ball frame], [block frame])){
            [block removeFromSuperview];
            [self.rectangles removeObject:block];
            dy = -dy;
            p.y += 2*dy;
            [ball setCenter:p];
            tscore += 5;
            [score setText:[NSString stringWithFormat:@"score: %d",tscore]];
        }
    }
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex != [alertView cancelButtonIndex]) {
        exit(0);
    }
    else{
        [paddle removeFromSuperview];
        [ball removeFromSuperview];
        [score removeFromSuperview];
        for(int i = (int)self.rectangles.count-1;i>=0;i--){
            UIView *block = self.rectangles[i];
            [block removeFromSuperview];
        }
        [self createPlayField];
    }
}
@end
